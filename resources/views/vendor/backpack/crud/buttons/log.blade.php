@if ($crud->hasAccess('log'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/log') }}" class="btn btn-xs btn-default"><i class="fa fa-bell"></i> {{ trans('backpack::base.log') }}</a>
@endif