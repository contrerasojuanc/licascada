@if ($crud->hasAccess('config'))
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/config') }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i> {{ trans('backpack::crud.config') }}</a>
@endif