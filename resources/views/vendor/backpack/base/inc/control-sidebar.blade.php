@if (Auth::check())
    
    @forelse($categories as $category)
        <div style="margin-top: 20px">
            <div class="col-md-12">
              <div class="box box-primary collapsed-box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">{{ $category->name }}</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @forelse($category->elements as $element)
                    <a href="#" name="element" id="{{$element->id}}" data-src="{{ asset('uploads')."\/".$element->image }}" data-text="{{$element->name}}" title="{{$element->name}}">
                          <img class="col-md-12 svg" src="{{ asset('uploads')."\/".$element->image }}" height="80px" />
                      </a>
                      <!--<input type="image" class="col-md-12" src="{{ asset('uploads').'/'.$element->image }}" name="element" id="{{$element->id}}" data-text="{{$element->name}}"/>-->
                    @empty
                      <div>{{ trans('backpack::base.no_elements') }}</div>
                    @endforelse
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    @empty
        {{ trans('backpack::base.no_categories') }}
    @endforelse
@endif
