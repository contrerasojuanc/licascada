@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="header">
              <span class="text-gray">{{ Auth::user()->name }} &nbsp;&nbsp;<a href="#"><i class="fa fa-circle text-success"></i> Online</a></span>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('backpack::base.administration') }}</li>
            <!-- ================================================ -->
            <!-- ==== Recommended place for admin menu items ==== -->
            <!-- ================================================ -->
            <!--<li><a href="{{ url(config('backpack.base.route_prefix').'/animation') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.animation') }}</span></a></li>-->
            <li><a href="{{ url(config('backpack.base.route_prefix').'/category') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.category') }}</span></a></li>
            <!--<li><a href="{{ url(config('backpack.base.route_prefix').'/connection') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.connection') }}</span></a></li>-->
            <li><a href="{{ url(config('backpack.base.route_prefix').'/element') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.element') }}</span></a></li>
            <li class="treeview">
                <a href="{{ url(config('backpack.base.route_prefix').'/environment') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.environment') }}</span></a>
                @yield('environment_menu')
            </li>
            <li><a href="{{ url(config('backpack.base.route_prefix').'/connection') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.connection') }}</span></a></li>
            <li><a href="{{ url(config('backpack.base.route_prefix').'/variable') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.variable') }}</span></a></li>
            <li><a href="{{ url(config('backpack.base.route_prefix').'/criterion') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.criterion') }}</span></a></li>
            <li><a href="{{ url(config('backpack.base.route_prefix').'/alarm') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.alarm') }}</span></a></li>
            <!--<li><a href="{{ url(config('backpack.base.route_prefix').'/file') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.file') }}</span></a></li>-->
            <!--<li><a href="{{ url(config('backpack.base.route_prefix').'/link') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.link') }}</span></a></li>-->
            <li><a href="{{ url(config('backpack.base.route_prefix').'/user') }}"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.users') }}</span></a></li>
            
          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ asset('uploads/Manual de Usuario.pdf') }}" target="_blank"><i class="fa fa-circle-o"></i> <span>{{ trans('backpack::base.help') }}</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
