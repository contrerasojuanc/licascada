<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>
      {{ isset($title) ? $title.' :: '.config('backpack.base.project_name').' Admin' : config('backpack.base.project_name').' Admin' }}
    </title>

    @yield('before_styles')

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/') }}/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/backpack/pnotify/pnotify.custom.min.css') }}">

    <style>
        .joint-element .element-tools {
                display: none;
                cursor: pointer
        }

        .joint-element:hover .element-tools {
                display: inline;
        }
    </style>
    
    <!-- BackPack Base CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/backpack/backpack.base.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/jointjs/css/joint.css') }}" />
    <script src="{{ asset('vendor/jointjs/js/jquery.js') }}"></script>
    <script src="{{ asset('vendor/jointjs/js/lodash.js') }}"></script>
    <script src="{{ asset('vendor/jointjs/js/backbone.js') }}"></script>
    <script src="{{ asset('vendor/jointjs/js/joint.js') }}"></script>
    
    @yield('after_styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition {{ config('backpack.base.skin') }} sidebar-mini fixed">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">{!! config('backpack.base.logo_mini') !!}</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">{!! config('backpack.base.logo_lg') !!}</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('backpack::base.toggle_navigation') }}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          @include('backpack::inc.menu')
        </nav>
      </header>

      <!-- =============================================== -->

      @include('backpack::inc.sidebar')

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="height: 100%;">
        <!-- Content Header (Page header) -->
         @yield('header')

        <!-- Main content -->
        <section class="content">

          @yield('content')

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer">
        @if (config('backpack.base.show_powered_by'))
            <div class="pull-right hidden-xs">
              {{ trans('backpack::base.powered_by') }} <a target="_blank" href="http://laravelbackpack.com">Laravel BackPack</a>
            </div>
        @endif
        {{ trans('backpack::base.handcrafted_by') }} <a target="_blank" href="{{ config('backpack.base.developer_link') }}">{{ config('backpack.base.developer_name') }}</a>.
      </footer>
      
      <!-- The Right Sidebar -->
        <aside class="control-sidebar control-sidebar-light">
          <!-- Content of the sidebar goes here -->
          @include('backpack::inc.control-sidebar')
        </aside>
        <!-- The sidebar's background -->
        <!-- This div must placed right after the sidebar for it to work-->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- Modal -->
  <div class="modal fade" id="modal-output" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <form action="{{ route('sending.element', ['id' => 'element_to_replace', 'value' => 'value_to_replace']) }}" method="POST" id="output-form" name="output-form">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{ trans('backpack::base.output_variable') }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{ csrf_field() }}
                    <input type="hidden" name="output-element" id="output-element" value="0">
                    <label for="output-variable">{{ trans('backpack::base.output_variable') }}</label>
                    <input type="text" class="form-control" id="output-variable-value" name="output-variable-value" placeholder="{{ trans('backpack::base.enter_output_variable') }}">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans('backpack::base.close') }}</button>
              <button type="submit" class="btn btn-primary pull-right" id="output-send" name="output-send">{{ trans('backpack::base.send') }}</button>
            </div>
          </form>
      </div>
      
    </div>
  </div>

    @yield('before_scripts')

    <!-- jQuery 2.2.0 -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.0.min.js"><\/script>')</script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('vendor/adminlte') }}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/pace/pace.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/plugins/fastclick/fastclick.js"></script>
    <script src="{{ asset('vendor/adminlte') }}/dist/js/app.js"></script>
    <script src="{{ asset('vendor/shorten') }}/jquery.shorten.js"></script>
    <script src="{{ asset('vendor/handlebars') }}/dist/handlebars.js"></script>
    <!-- page script -->
    <script type="text/javascript">
        // To make Pace works on Ajax calls
        $(document).ajaxStart(function() { Pace.restart(); });

        // Ajax calls should always have the CSRF token attached to them, otherwise they won't work
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        // Set active state on menu element
        var current_url = "{{ url(Route::current()->getUri()) }}";
        $("ul.sidebar-menu li a").each(function() {
          if ($(this).attr('href').startsWith(current_url) || current_url.startsWith($(this).attr('href')))
          {
            $(this).parents('li').addClass('active');
          }
        });
    </script>
    
    <script type="text/javascript">
    
    var width = '100%';
    var height = '475';
    var gridSize = 1;

    var graph = new joint.dia.Graph;

    var paper = new joint.dia.Paper({
        el: $('#canvas'),
        width: width,
        height: height,
        model: graph,
        gridSize: gridSize,
        restrictTranslate: true
    });

    $('[name=element]').on('click', function() {
        var im = new joint.shapes.tm.Actor({
            position: { x: 50, y: 50 },
            size: { width: 80, height: 80 },
            attrs: {
                text: { text: $(this).data('text') },
                image: { 'xlink:href': $(this).data('src'), width: 80, height: 80 },
                type: { value: $(this).prop('id') }
            }
        });
        graph.addCells([im]);
        im.updateAttributes(0);
    });
    
    paper.on('cell:pointerdblclick', 
        function(cellView, evt, x, y) { 
            $('#output-element').val(cellView.model.attr('type').value);
            $('#modal-output').modal();
        }
    );
    
    $('#output-form').on('submit', function(event) {
        event.preventDefault();
        $('#modal-output').modal('hide');
        var url_to_replace = $('#output-form').attr('action').replace("element_to_replace", $('#output-element').val());
        var url = url_to_replace.replace("value_to_replace", $('#output-variable-value').val()); 
        $.ajax({
            method: "GET",
            url: url
        });
    });
    
    //Code for zooming
//    paper.on('mousewheel DOMMouseScroll', function (evt) {
//
//        evt.preventDefault();
//        var p = clientToLocalPoint({
//            x: evt.clientX,
//            y: evt.clientY
//        });
//        var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
//        var currentScale = V(paper.viewport).scale().sx;
//        var newScale = currentScale + delta / 50;
//
//        if (newScale > 0.4 && newScale < 2) {
//            paper.setOrigin(0, 0);
//            paper.scale(newScale, newScale, p.x, p.y);
//        }
//    });

    /////////////////////////////////////
    /////////////////////////////////////
    //Tools on hover
    /////////////////////////////////////
    ///////////////////////////////////// 
    
    joint.shapes.tm = {};

    joint.shapes.tm.toolElement = joint.shapes.basic.Generic.extend({

        toolMarkup: ['<g class="element-tools">',
            '<g class="element-tool-remove"><circle fill="red" r="11"/>',
            '<path transform="scale(.8) translate(-16, -16)" d="M24.778,21.419 19.276,15.917 24.777,10.415 21.949,7.585 16.447,13.087 10.945,7.585 8.117,10.415 13.618,15.917 8.116,21.419 10.946,24.248 16.447,18.746 21.948,24.248z"/>',
            '<title>Remove this element from the model</title>',
            '</g>',
            '<g class="element-tool-resize" transform="translate(80,80) rotate(45)"><circle fill="blue" r="11"/>',
            '<path d="M-7.603665602211981,0.08316232346240326 L-2.991431144955355,-4.529094529471209 L-2.991431144955355,-2.2229722351540477 L3.692903048847441,-2.2229722351540477 L3.692903048847441,-4.529094529471209 L8.305135373182619,0.08316232346240326 L3.692903048847441,4.695394647797265 L3.692903048847441,2.38927821901461 L-2.991431144955355,2.38927821901461 L-2.991431144955355,4.695394647797265 L-7.603665602211981,0.08316232346240326 z" />',
            '<title>Resize this element</title>',
            '</g>',
            '</g>'].join(''),

        defaults: joint.util.deepSupplement({
            attrs: {
                
            },
        }, joint.shapes.basic.Generic.prototype.defaults)

    });

    joint.shapes.tm.Actor = joint.shapes.tm.toolElement.extend({

        dataURLPrefix: 'data:image/svg+xml;utf8,', // a dataURL with $color to be replaced on runtime
        
        markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><image/><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>',

        defaults: joint.util.deepSupplement({

            type: 'tm.Actor',
            attrs: {
                rect: { fill: 'transparent', stroke: 'transparent', 'stroke-width': 0, 'follow-scale': true, width: 80, height: 80 },
                text: { ref: 'rect', class: '', 'font-weight': 400, 'font-size': 'small', fill: 'black', 'text-anchor': 'middle', 'ref-x': .5, 'ref-y': -30, },
                image: { 'ref-x': 0, 'ref-y': 0, ref: 'rect', width: 80, height: 80 },
                type: { value: 0 }
            },
            size: { width: 80, height: 80 }
        }, joint.shapes.tm.toolElement.prototype.defaults),
        // Function to update attributes when new data arrives 
        updateAttributes: function(value) {
            // Type means id from catalog of elements
            var type = this.attr('type').value;
            var svg_src = $('a#'+type).find('svg').get(0);
            var svg_xml = (new XMLSerializer()).serializeToString(svg_src);
            
            for (var variable in value) {           
                //Change value according to criteria
                var re = new RegExp('\\'+variable,'g');
                svg_xml = svg_xml.replace(re, value[variable]);
            }
            
            this.attr('image/xlink:href', this.dataURLPrefix + encodeURIComponent(svg_xml));
        },
    });

    joint.shapes.tm.ToolElementView = joint.dia.ElementView.extend({

        initialize: function() {

            joint.dia.ElementView.prototype.initialize.apply(this, arguments);
        },

        render: function () {

            joint.dia.ElementView.prototype.render.apply(this, arguments);

            this.renderTools();
            this.update();

            return this;
        },

        renderTools: function () {

            var toolMarkup = this.model.toolMarkup || this.model.get('toolMarkup');

            if (toolMarkup) {

                var nodes = V(toolMarkup);
                V(this.el).append(nodes);

            }

            return this;
        },

        pointerclick: function (evt, x, y) {

            this._dx = x;
            this._dy = y;
            this._action = '';

            var className = evt.target.parentNode.getAttribute('class');

            switch (className) {

                case 'element-tool-remove':
                    this.model.remove();
                    break;
                    
                default:
            }
            joint.dia.CellView.prototype.pointerclick.apply(this, arguments);
        },
        
        pointerdown: function (evt, x, y) {
            
            var className = evt.target.parentNode.getAttribute('class');

            switch (className) {

                case 'element-tool-resize':
                    this._action = 'resize';
                    this._element = evt.target.parentNode;
                    this._dx = x;
                    this._dy = y;
                    break;
                case 'element-tool-remove':
                    this._action = 'remove';
                    break;
                default:
            }
            joint.dia.CellView.prototype.pointerdown.apply(this, arguments);
        },
        
        pointermove: function (evt, x, y) {

            let positionX = 0;
            let positionY = 0;
            positionX = this.model.attr('rect/width');
            positionY = this.model.attr('rect/height');
            let moveX = x - this._dx;
            let moveY = y - this._dy;
            switch (this._action) {
                case 'resize':
                    this.model.attr('image/width', positionX + moveX);
                    this.model.attr('image/height', positionY + moveY);
                    this.model.resize(positionX + moveX, positionY + moveY );
                    this.model.attr('rect/width', positionX + moveX);
                    this.model.attr('rect/height', positionY + moveY);
                    this._dx = x;
                    this._dy = y;
                    this._element.setAttribute('transform',"translate("+(positionX + moveX)+","+(positionY + moveY)+") rotate(45)");
                    break;
                case 'remove':
                    this.model.remove();
                    break;
                default:
                    this.model.position( x - (positionX / 2), y - (positionY / 2) );
            }
            
            joint.dia.CellView.prototype.pointermove.apply(this, arguments);
        },
        
        pointerup: function (evt, x, y) {

            this._action = '';
            this._element = '';
            this._dx = x;
            this._dy = y;

            joint.dia.CellView.prototype.pointerup.apply(this, arguments);
        }

    });

    joint.shapes.tm.ActorView = joint.shapes.tm.ToolElementView;

    /////////////////////////////////////
    /////////////////////////////////////
    //Tools on hover
    /////////////////////////////////////
    /////////////////////////////////////
    
    
//    var paper = new joint.dia.Paper({
//        ...
//        interactive: function(cellView, methodName) {
//            if (cellView.model.get('isInteractive') === false) return false;
//            return true;
//        }
//        ...
//     })
//
//    var rect = new joint.shapes.basic.Rect({
//       ...
//       isInteractive: false
//       ...
//    })

    //Avoid cache img on svg
    $(document).ready(function(){  
        $('img').each(function(){  
            $(this).attr('src',$(this).attr('src')+ '?' + (new Date()).getTime());  
        });
        
        /*
         * Replace all SVG images with inline SVG
         */
        jQuery('a img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                $svg = $svg.attr("height", '80px');
                $svg = $svg.css("padding-bottom", '10px');
                $svg = $svg.attr("preserveAspectRatio", "xMinYMin meet");

                // Replace image with new SVG
                $img.replaceWith($svg);

            });

        });
        
        /////////////////////////////////////
        /////////////////////////////////////
        //Update loop proccess
        /////////////////////////////////////
        /////////////////////////////////////
        (function updater() {

            var data = {};
            var url = "{{ route('adquisition.environment', ['id' => isset($environment->id) ? $environment->id : '0']) }}";
            
            if(graph.getElements().length == 0){
                setTimeout(updater,{{ isset($environment) ? $environment->frequency : '1000' }});
            } else {
                $.ajax({
                        method: "GET",
                        url: url
                })
                .done(function(data, textStatus, jqXHR) {
                    if(data.success) {
                        _.each(graph.getElements(), function(el) {
                            el.updateAttributes(data.value);
                        });
                    }
                })
                .always(function() {
                    setTimeout(updater, {{ isset($environment) ? $environment->frequency : '1000' }});
                });
            }
        })();
        /////////////////////////////////////
        /////////////////////////////////////
        //Update loop process
        /////////////////////////////////////
        /////////////////////////////////////
        
        /////////////////////////////////////
        /////////////////////////////////////
        //Load graph
        /////////////////////////////////////
        /////////////////////////////////////

        // Load back the json to the diagram:
        @if(isset($environment) && $environment->json != '')
            //console.log('{!! $environment->json !!}');
            graph.fromJSON(JSON.parse('{!! $environment->json !!}'));
        @endif

        /////////////////////////////////////
        /////////////////////////////////////
        //Load graph
        /////////////////////////////////////
        /////////////////////////////////////


        /////////////////////////////////////
        /////////////////////////////////////
        //Save graph
        /////////////////////////////////////
        /////////////////////////////////////
        $('#save-design').on('click', function() {

            var json = JSON.stringify(graph);
            console.log(json);

            //Html elements from environmentconfig.blade.php
            $('[name="json"]').val(json);
            $('[name="environment_form"]').submit();

        });
        /////////////////////////////////////
        /////////////////////////////////////
        //Save graph
        /////////////////////////////////////
        /////////////////////////////////////

    });  
  </script>

    @include('backpack::inc.alerts')

    @yield('after_scripts')

    <!-- JavaScripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
