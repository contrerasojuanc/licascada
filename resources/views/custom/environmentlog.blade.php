@extends('backpack::layout')

@section('after_styles')
	<!-- DATA TABLES -->
    <link href="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('header')
        <section class="content-header">
	  <h1>
	    <span class="text-capitalize">{{ trans('backpack::base.environment') }}</span>
	    <small>{{ trans('backpack::base.log') }} <span class="text-lowercase">{{ $environment->name }}</span></small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li class="active">{{ trans('backpack::base.log') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
<!-- Default box -->
  <div class="box">
    <div class="box-header">
    </div>
    <div class="box-body">

		<table id="crudTable" class="table table-bordered table-striped display">
                <thead>
                  <tr>
                      <th>Variable</th>
                      <th>Datetime</th>
                      <th>Value</th>
                  </tr>
                </thead>
        <tbody>
            @foreach($logs as $log)
            @if($log->is_alarmed)
            <tr class="alert alert-danger">
            @else
            <tr>
            @endif
                <td>
                    {{ isset($log->variable->name) ? $log->variable->name : '-' }}
                </td>
                <td>
                    {{ $log->created_at }}
                </td>
                <td>
                    {{ $log->value }}
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
          
        </tfoot>
      </table>

    </div><!-- /.box-body -->


  </div><!-- /.box -->
  <a id="link-reload" name="link-reload" href="{{ route('log.environment', $environment->id) }}" style="display: none;">Refresh</a>
@endsection

@section('after_scripts')
	<!-- DATA TABES SCRIPT -->
    <script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    
    <script>
        var table = $("#crudTable").DataTable({
        //"ajax": "{{ route('log.environment', $environment->id) }}",
        "aaSorting": [],
        "pageLength": 100,
        "language": {
              "emptyTable":     "{{ trans('backpack::crud.emptyTable') }}",
              "info":           "{{ trans('backpack::crud.info') }}",
              "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
              "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
              "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
              "thousands":      "{{ trans('backpack::crud.thousands') }}",
              "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
              "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
              "processing":     "{{ trans('backpack::crud.processing') }}",
              "search":         "{{ trans('backpack::crud.search') }}",
              "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
              "paginate": {
                  "first":      "{{ trans('backpack::crud.paginate.first') }}",
                  "last":       "{{ trans('backpack::crud.paginate.last') }}",
                  "next":       "{{ trans('backpack::crud.paginate.next') }}",
                  "previous":   "{{ trans('backpack::crud.paginate.previous') }}"
              },
              "aria": {
                  "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                  "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
              }
          },

      });
      
      $('document').on('ready', function() {
          setTimeout(location.reload(),{{ isset($environment) ? $environment->frequency : '1000' }});
      });
            
    </script>
@endsection