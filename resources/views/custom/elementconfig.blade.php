@extends('backpack::layout')

@section('after_styles')
<link href="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<style>
    svg *:hover {
        fill: white !important;
        stroke: black !important;
	opacity: 0.5;
	transition: opacity 0.3s;
    }
    
    .input-group-unstyled .input-group-addon {
        border: 0px;
        background-color: transparent;
        margin: 0;
        padding: 0px 0px 0px 5px;
    }
    
    .box-body .fa {
        text-shadow: 1px 1px 1px #ccc;
        font-size: 1.5em;
        vertical-align: bottom;
    }
    .box-body .fa:hover {
        text-shadow: 2px 2px 2px #ccc;
        font-size: 1.5em;
        cursor: pointer;
        vertical-align: bottom;
    }
    .box-body .fa-plus {
        color: #00FF00;
        position: fixed;
    }
    .box-body .fa-plus:hover {
        color: #00CC00;
    }
    .box-body .fa-times {
        color: #FF0000;
    }
    .box-body .fa-times:hover {
        color: #CC0000;
    }
    
    #map-catcher{
        position:absolute;
        left:0px;
        top:0px;
        height:100%;
        width:100%;
    }
</style>

@endsection

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.svg_config') }}<small>{{ trans('backpack::base.svg_config_usage') }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.svg_config') }}</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-6">
              <div class="box box-default box-solid box-scroll" style="margin-bottom: 0px; padding-bottom: 0px;">
                <div class="box-header with-border">
                  <h3 class="box-title">1. {{ trans('backpack::base.select_path') }}</h3>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body svg-body" style="padding-left: 0px !important; padding-right: 0px !important;">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <input id="parent_button" type="button" value="{{ trans('backpack::base.parent_graph') }}" class="btn btn-block" />
                    </div>
                        <img class="col-md-12 svg" src="{{ asset('uploads')."\/".$element->image }}" height="50%" />
                    </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
        {{--
        <div class="col-md-4">
              <div class="box box-default box-solid box-scroll" style="margin-bottom: 0px; padding-bottom: 0px;">
                <div class="box-header with-border">
                  <h3 class="box-title">SVG fragments</h3>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="overflow: auto;">
                    @foreach($vals as $val)
                        @if($val['type'] == 'open' || $val['type'] == 'complete')
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-default btn-xs fragment" style="margin-left: {{ isset($val['level'])? ( $val['level']-1 ) * 2:0 }}em"
                                        @if(isset($val['attributes']))
                                            @foreach($val['attributes'] as $nameAtttr => $valueAttr)
                                                data-{{ $nameAtttr }}="{{ $valueAttr }}"
                                            @endforeach
                                        @endif
                                    > 
                                        {{ $val['tag'] }}
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
        
        <div class="col-md-5">
              <div class="box box-default box-solid box-scroll" style="margin-bottom: 0px; padding-bottom: 0px;">
                <div class="box-header with-border">
                  <h3 class="box-title">2. {{ trans('backpack::base.set_criteria') }}</h3>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="overflow-y: auto; overflow-x:hidden;">
                    <div id="variablesAttributes">
                    </div>
                    <table id="variablesTable" class="table table-condensed table-hover">
                        <thead>
                            <th>{{ trans('backpack::base.criteria') }}</th>
                            <th>{{ trans('backpack::base.fill') }}</th>
                            <th>{{ trans('backpack::base.translate') }}</th>
                            <th>{{ trans('backpack::base.rotate') }}</th>
                            <th>{{ trans('backpack::base.scale') }}</th>
                            <th>{{ trans('backpack::base.text') }}</th>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
        --}}
        <div class="col-md-6">
              <div class="box box-default box-solid box-scroll" style="margin-bottom: 0px; padding-bottom: 0px;">
                <div class="box-header with-border">
                  <h3 class="box-title">2. {{ trans('backpack::base.set_variable') }}</h3>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="overflow-y: auto; overflow-x:hidden;">
                    {!! Form::open(['route' => ['save.config', $element->id], 'method' => 'post']) !!}
                        <div id="fragmentsForm">
                            <div id="fragmentsAttributes">
                            </div>
                        </div>
                        <div id="variablesForm" style="display: none;">
                        </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
        </div>
    </div>
    
<div id="new-row-template" style="display: none;">
    <div class="row">
        <div class="col-md-4" style="word-wrap: break-word;">
            <span>{{ trans('backpack::base.attribute') }}</span>
        </div>
        <div class="col-md-8 fragmentDetail" style="word-wrap: break-word;">
            <span>{{ trans('backpack::base.value') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="word-wrap: break-word;">
            <div class="form-group">
                <input type="text" class="form-control" id="text-name" />
            </div>
        </div>
        <div class="col-md-8 fragmentDetail" style="word-wrap: break-word;">
            <div class="input-group input-group-unstyled">
                <input type="text" id="text-value" class="form-control"/>
                <span class="input-group-addon"><i class="fa fa-plus" style="position: relative;" name="new-row"></i></span>
            </div>
        </div>
    </div>
    <br>
</div>
    
<div id="new-row-variable-template" style="display: none;">
    <form class="form-horizontal">
        <div style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="criteria">{{ trans('backpack::base.criteria') }}:</label>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-4">
                            <select id="criteria" class="form-control" style="display: inline;">
                                <option value="=">=</option>
                                <option value=">">></option>
                                <option value="<"><</option>
                                <option value=">=">>=</option>
                                <option value="<="><=</option>
                            </select>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="criteria_value" placeholder="{{ trans('backpack::base.value') }}"  style="display: inline;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="fill">{{ trans('backpack::base.fill') }}:</label>
                <div class="col-sm-7">
                    <input type="color" class="form-control" id="fill" placeholder="{{ trans('backpack::base.hex_color') }}">
                </div>
            </div>
        </div>
        <div style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="translate">{{ trans('backpack::base.translate') }}:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="translate" placeholder="{{ trans('backpack::base.coordinates') }}">
                </div>
            </div>
        </div>
        <div style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="rotate">{{ trans('backpack::base.rotate') }}:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="rotate" placeholder="{{ trans('backpack::base.angle') }}">
                </div>
            </div>
        </div>
        <div style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="scale">{{ trans('backpack::base.scale') }}:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="scale" placeholder="{{ trans('backpack::base.proportion') }}">
                </div>
            </div>
        </div>
        <div class="variableDetail" style="word-wrap: break-word;">
            <div class="form-group">
                <label class="control-label col-sm-4" for="text">{{ trans('backpack::base.text') }}:</label>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="text" class="form-control" id="text" placeholder="{{ trans('backpack::base.label') }}">
                            </div>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <div class="input-group input-group-unstyled">
                                <span class="input-group-addon"><i class="fa fa-plus" style="position: relative;" name="new-criteria"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br>
</div>

<div id="delete-row-template" style="display: none;">
    <div class="row">
        <div class="col-md-4" style="word-wrap: break-word;">
            <i class="fa fa-times" name="delete-row"></i>
            <strong>
                @{{name}}
            </strong>
            <input type="hidden" name="name[]" value="@{{name}}" />
        </div>
        <div class="col-md-8 fragmentDetail" style="word-wrap: break-word;">
            <div>@{{value}}</div>
            <input type="hidden" name="value[]" value="@{{value}}" />
        </div>
    </div>
</div>

<div id="save-template" style="display: none;">
    <div class="text-right" style="margin-top: 30px;">
            <input type="hidden" name="svg-for-update" />
            <button type="submit" class="btn btn-success btn-block" data-style="zoom-in">
                <span class="ladda-label">{{ trans('backpack::crud.save') }}</span>
            </button>
    </div>
</div>
    
@endsection

@section('after_scripts')
<script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
<script>
    var pathSelected = null;
    $( document ).ready(function() { 
        boxSize();
        
        $( window ).resize(function() {
            boxSize();
        });
        
        var table = $("#variablesTable").DataTable({
            "info": false,
            "ordering": false,
            "paging": false,
            "searching": false
        });
        
        $(document).on('click', '.fragment', function(event){
            event.preventDefault();                        
            $('#fragmentsAttributes').html('');
            $.each($(this).data(), function(i, v) {
                var row = '<div class="row"><div class="col-md-3" style="word-wrap: break-word;"><strong>'+i+'</strong></div><div class="col-md-9 fragmentDetail" style="word-wrap: break-word;">'+v+'</div></div>';
                $('#fragmentsAttributes').append(row);
            });
            $(".fragmentDetail").shorten();
        });
        
        /*
         * Replace all SVG images with inline SVG
         */
        jQuery('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                $svg = $svg.attr("height", $(".box-scroll .box-body").height());
                $svg = $svg.attr("preserveAspectRatio", "xMinYMin meet");

                // Replace image with new SVG
                $img.replaceWith($svg);

                // Add an handler
                jQuery('path,text,g').each(function() {
                    jQuery(this).click(function() {

                        selectedFragment(this);
                        
                    });                       
                });
            });

        });
        
        $(document).on('click', '[name^=new-criteria]', function(event){
            if($('#criteria').val() != '') {
                $('#variablesForm').append('<input type="hidden" name="criteria[]" value="'+$('#criteria').val()+$('#criteria_value').val()+'"/>');
                $('#variablesForm').append('<input type="hidden" name="fill[]" value="'+$('#fill').val()+'"/>');
                $('#variablesForm').append('<input type="hidden" name="translate[]" value="'+$('#translate').val()+'"/>');
                $('#variablesForm').append('<input type="hidden" name="rotate[]" value="'+$('#rotate').val()+'"/>');
                $('#variablesForm').append('<input type="hidden" name="scale[]" value="'+$('#scale').val()+'"/>');
                $('#variablesForm').append('<input type="hidden" name="text[]" value="'+$('#text').val()+'"/>');
                table.row.add( [
                    $('#criteria').val()+$('#criteria_value').val(),
                    $('#fill').val(),
                    $('#translate').val(),
                    $('#rotate').val(),
                    $('#scale').val(),
                    $('#text').val(),
                ] ).draw( false );
            }
        });
        
        $(document).on('click', '[name^="delete-row"]', function(event){
            var name = $(this).closest('.row').find('[name^="name"]').val();
            
            //Removing attribute on svg
            pathSelected.attributes.removeNamedItem(name);

            $(this).closest('.row').remove();
            
            updateSVG();
        });
        
        $(document).on('click', '[name^="new-row"]', function(event){
            var deleteRow = $("#delete-row-template").clone();
            var name = $('#text-name').val();
            var value = $('#text-value').val();
            
            if(name && value) {
                //If it has the attribute or has the content
                if (pathSelected.hasAttribute(name) || name == 'content' ) {
                    var $rowElement = $('#fragmentsAttributes').find('[name^="name"][value="'+name+'"]').closest('.row');
                    $rowElement.find('.fragmentDetail').find('div').html(value);
                    $rowElement.find('input[name^="value"]').val(value);
                } else {
                    template = Handlebars.compile(deleteRow.html());
                    context = {name:name, value:value};
                    deleteRow = template(context);
                    $('#fragmentsAttributes').append(deleteRow);
                }
                
                if(name == 'content') {
                    $(pathSelected).html(value);
                } else {
                    //Adding attribute on svg
                    var newAttribute = document.createAttribute(name);
                    newAttribute.value = value;
                    pathSelected.attributes.setNamedItem(newAttribute);
                }
                
                $('#text-name').val('');
                $('#text-value').val('');
                updateSVG();
            }
        });
        
        $('#parent_button').on('click', function() {
            selectedFragment($(pathSelected).parent().closest('g').get(0));
        });
            
        function selectedFragment(element) {
            pathSelected = element;
            var newRow = $("#new-row-template").html();
            $('#fragmentsForm').find('button').remove();
            $('#fragmentsAttributes').html('');
            $('#fragmentsAttributes').append(newRow);

            var newVariableRow = $("#new-row-variable-template").html();
            $('#variablesAttributes').html(newVariableRow);
            $('#variablesForm').html('');
            table.clear();
            table.draw(false);

            $.each(element.attributes, function(i, attrib){
                var deleteRow = $("#delete-row-template").clone();
                var name = attrib.name;
                var value = attrib.value;
                template = Handlebars.compile(deleteRow.html());
                context = {name:name, value:value};
                deleteRow = template(context);
                $('#fragmentsAttributes').append(deleteRow);
            });

            //Content
            var deleteRow = $("#delete-row-template").clone();
            var name = 'content';
            var value = $(element).html();
            template = Handlebars.compile(deleteRow.html());
            context = {name:name, value:value};
            deleteRow = template(context);
            $('#fragmentsAttributes').append(deleteRow);

            $(".fragmentDetail").shorten();
            var save = $("#save-template").clone();
            template = Handlebars.compile(save.html());
            $('#fragmentsAttributes').after(template());
        }
    });
    
    function boxSize() {
        var height = ($(window).height() - $(".main-header").height() - $(".main-footer").height() - $(".box-scroll").offset().top);
        $(".box-scroll").height(height);
        $(".box-scroll .box-body").outerHeight( $(".box-scroll").height() - $(".box-header").outerHeight() );
    }
    
    function updateSVG() {
        var svg = $('svg').get(0);
        var svg_xml = (new XMLSerializer()).serializeToString(svg);
        $('[name="svg-for-update"]').val(svg_xml);
    }
</script>

@endsection