@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.environment') }}<small>{{ trans('backpack::base.design') }}</small>
      </h1>
      {{-- Alerts --}}
        @foreach(['success', 'info', 'warning', 'danger'] as $message_type)
            @if(Session::has($message_type))
                <span class="alert alert-{{ $message_type }} alert-dismissable inline">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ Session::get($message) }}
                </span>
            @endif
        @endforeach    
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::design') }}</li>
      </ol>
    </section>
@endsection

@section('environment_menu')
    <ul class="treeview-menu">
        <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-plus"></i> <span>{{ trans('backpack::base.add') }}</span></a></li>
        <li><a href="#" id="save-design"><i class="fa fa-save"></i> <span>{{ trans('backpack::base.save') }}</span></a></li>
        <li><a href="{{ url(config('backpack.base.route_prefix').'/environment') }}"><i class="fa fa-arrow-left"></i> <span>{{ trans('backpack::base.back') }}</span></a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">{{ $environment->name }}</div>
                </div>
                <div class="box-body" id="canvas" style="height: inherit;">
                    
                </div>
                {!! Form::open(['route' => ['save.environment', $environment->id], 'method' => 'post', 'name' => 'environment_form']) !!}
                    <input type="hidden" name="json" value="{{ $environment->json }}">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
