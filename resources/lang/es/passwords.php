<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña deben ser al menos de seis caracteres e igual a la confirmación.',
    'reset' => 'Tu contraseña ha sido reseteada!',
    'sent' => 'Nosotros ahora hemos enviado un correo electrónico con el vínculo para resetear tu contraseña!',
    'token' => 'Este token para resetear la contraseña es inválido.',
    'user' => "No podemos encontrar un usuario con esa dirección de correo electrónico.",

];
