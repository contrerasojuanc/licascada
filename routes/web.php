<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Admin Interface Routes
Route::group(['prefix' => 'admin', 'middleware' => ['language', 'auth', 'control-side-bar'] ], function()
{
    Route::get('home', 'Admin\EnvironmentCrudController@index');

// CRUD: Define the resources for the entities you want to CRUD.
    CRUD::resource('animation', 'Admin\AnimationCrudController');
    CRUD::resource('category', 'Admin\CategoryCrudController');
    CRUD::resource('connection', 'Admin\ConnectionCrudController');
    CRUD::resource('element', 'Admin\ElementCrudController');
        Route::any('element/{id}/sending/{value}', 'Admin\ElementCrudController@sending')->name('sending.element');
        Route::get('element/{id}/config', 'Admin\ElementCrudController@config');
        Route::any('element/{id}/save', 'Admin\ElementCrudController@save')->name('save.config');
    CRUD::resource('environment', 'Admin\EnvironmentCrudController');
        Route::get('environment/{id}/config', 'Admin\EnvironmentCrudController@config')->name('config.environment');
        Route::get('environment/{id}/log', 'Admin\EnvironmentCrudController@log')->name('log.environment');
        Route::post('environment/{id}/save', 'Admin\EnvironmentCrudController@save')->name('save.environment');
        Route::get('environment/{id}/logged/{elementId}/{value}', 'Admin\EnvironmentCrudController@logged')->name('logged.environment');
        Route::get('environment/{id}/adquisition', 'Admin\EnvironmentCrudController@adquisition')->name('adquisition.environment');
    CRUD::resource('file', 'Admin\FileCrudController');
    CRUD::resource('link', 'Admin\LinkCrudController');
    CRUD::resource('user', 'Admin\UserCrudController');
    CRUD::resource('variable', 'Admin\VariableCrudController');
    CRUD::resource('alarm', 'Admin\AlarmCrudController');
    CRUD::resource('criterion', 'Admin\CriterionCrudController');
});

Route::get('home', 'Admin\EnvironmentCrudController@index');
Route::get('/', function() {
    return redirect()->route('login');
});
Route::get('login', function() {
    return redirect()->route('login');
});
Route::get('/{language}', 'LayoutController@changeLanguage')->name('language');
