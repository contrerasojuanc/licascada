<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variable extends Model
{
	use CrudTrait;
        use SoftDeletes;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'variables';
	protected $primaryKey = 'id';
	public $timestamps = false;
	//protected $guarded = ['id'];
	protected $fillable = [ 
                                'name', 
                                'description', 
                                'unit', 
                                'function_code', 
                                'registers_number', 
                                'should_output', 
                                'value_output', 
                                'connection_id',
                                'start_address', 
        ];
	// protected $hidden = [];
    // protected $dates = [];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
        public function getType() {
            $type = '';
            switch ($this->function_code) {
                case 1:
                    $type = 'Digital I/O';
                    break;
                case 2:
                    $type = 'Digital Input';
                    break;
                case 3:
                    $type = 'Register I/O';
                    break;
                case 4:
                    $type = 'Register Input';
                    break;
                case 5:
                    $type = 'Write Digital I/O';
                    break;
                case 6:
                    $type = 'Write Register I/O';
                    break;
            }
            return $type;
        }
        
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
        public function connection()
        {
             return $this->belongsTo('App\Models\Connection');
        }
        public function criteria()
        {
             return $this->hasMany('App\Models\Criterion', 'variable_id', 'id');
        }
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
