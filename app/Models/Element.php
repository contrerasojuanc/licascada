<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Element extends Model
{
	use CrudTrait;
        use SoftDeletes;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'elements';
	protected $primaryKey = 'id';
	//public $timestamps = false;
	 protected $guarded = ['id'];
	protected $fillable = [
            'name',
            'image',
            'categories_id',
            'type',
        ];
//	 protected $hidden = [''];
    // protected $dates = [];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
        public static function boot()
        {
            static::deleting(function($obj) {
                \Storage::disk('uploads')->delete($obj->image);
            });
        }
        
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function category()
        {
             return $this->belongsTo('App\Models\Category', 'categories_id', 'id');
        }
	
	/*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
        public function setImageAttribute($value)
        {
            $attribute_name = "image";
            $disk = "uploads";
            $destination_path = "svg";

            // if the image was erased
            if ($value==null) {
                // delete the image from disk
                \Storage::disk($disk)->delete($this->image);

                // set null in the database column
                $this->attributes[$attribute_name] = null;
            }
            else {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
            }
        }
}
