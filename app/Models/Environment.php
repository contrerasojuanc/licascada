<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Environment extends Model
{
	use CrudTrait;
        use SoftDeletes;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'environments';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	 protected $guarded = ['id'];
	protected $fillable = [
            'name',
            'connection_id',
            'json',
        ];
	// protected $hidden = [];
    // protected $dates = [];

	/*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/
        public static function boot()
        {
            static::deleting(function($obj) {
                \Storage::disk('uploads')->delete($obj->image);
            });
        }
        
	/*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
        public function logs()
        {
            return $this->hasMany(Log::class);
        }
        
        public function connection()
        {
            return $this->belongsTo(Connection::class);
        }
               
       /*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/
        public function getJsonAttribute() {
            if(Storage::exists('evironment'.$this->attributes['id'].'.json')) {
                return Storage::get('evironment'.$this->attributes['id'].'.json');
            }
            return '';
        }
	/*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
        public function setImageAttribute($value)
        {
            $attribute_name = "image";
            $disk = "uploads";
            $destination_path = "svg";

            // if the image was erased
            if ($value==null) {
                // delete the image from disk
                \Storage::disk($disk)->delete($this->image);

                // set null in the database column
                $this->attributes[$attribute_name] = null;
            }
            else {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
            }
        }
}
