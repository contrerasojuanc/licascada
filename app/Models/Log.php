<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
      'variable_id',  
      'value',
    ];
    
    public function environment() 
    {
        return $this->belongsTo(Environment::class);
    }
    
    public function variable() 
    {
        return $this->belongsTo(Variable::class);
    }
}
