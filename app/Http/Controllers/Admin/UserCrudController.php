<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;

class UserCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->middleware(function ($request, $next) {
            if (session()->has('applocale') && in_array(session()->get('applocale'), ['es', 'en'])) {
                \App::setLocale(session()->get('applocale'));
                $this->crud->setEntityNameStrings(trans('backpack::crud.user_label'), trans('backpack::crud.users_label'));
            }
            return $next($request);
        });
        
        $this->crud->setModel("App\Models\User");
        $this->crud->setRoute("admin/user");
        $this->crud->setEntityNameStrings(trans('backpack::crud.user_label'), trans('backpack::crud.users_label'));

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();

		// ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumns(['password', 'remember_token']); // remove an array of columns from the stack
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']);
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though: 
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable(); 

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
            // insert item in the db
            if ($request->input('password')) {
                $item = $this->crud->create(\Request::except(['redirect_after_save']));
                // now bcrypt the password
                $item->password = bcrypt($request->input('password'));
                $item->save();
            } else {
                $item = $this->crud->create(\Request::except(['redirect_after_save', 'password']));
            }
            
            // show a success message
            \Alert::success(trans('backpack::crud.insert_success'))->flash();
            // save the redirect choice for next time
            $this->setSaveAction();
            return $this->performSaveAction($item->getKey());
	}

	public function update(UpdateRequest $request)
	{
            $dataToUpdate = \Request::except(['redirect_after_save', 'password']);
            //encrypt password
            if ($request->input('password')) {
                $dataToUpdate['password'] = bcrypt($request->input('password'));
            }
            // update the row in the db
            $this->crud->update(\Request::get($this->crud->model->getKeyName()), $dataToUpdate);

            // show a success message
            \Alert::success(trans('backpack::crud.update_success'))->flash();
            // save the redirect choice for next time
            $this->setSaveAction();
            return $this->performSaveAction();
	}
}
