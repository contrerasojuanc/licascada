<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AlarmRequest as StoreRequest;
use App\Http\Requests\AlarmRequest as UpdateRequest;

class AlarmCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->middleware(function ($request, $next) {
            if (session()->has('applocale') && in_array(session()->get('applocale'), ['es', 'en'])) {
                \App::setLocale(session()->get('applocale'));
                $this->crud->setEntityNameStrings(trans('backpack::crud.alarm_label'), trans('backpack::crud.alarms_label'));
            }
            return $next($request);
        });
        
        $this->crud->setModel("App\Models\Alarm");
        $this->crud->setRoute("admin/alarm");
        $this->crud->setEntityNameStrings(trans('backpack::crud.alarm_label'), trans('backpack::crud.alarms_label'));

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();                

                $this->crud->removeField('environment_id');
                $this->crud->removeField('element_id');
                $this->crud->removeField('variable_id');
                $this->crud->addField([    // SELECT
                        'label' => trans('backpack::crud.environment_label'),
                        'type' => 'select2',
                        'name' => 'environment_id',
                        'entity' => 'environment',
                        'attribute' => 'name',
                        'model' => "App\Models\Environment"
                ]);
                $this->crud->addField([    // SELECT
                        'label' => trans('backpack::crud.variable_label'),
                        'type' => 'select2',
                        'name' => 'variable_id',
                        'entity' => 'variable',
                        'attribute' => 'name',
                        'model' => "App\Models\Variable"
                ]);
                $this->crud->addField([
                    'name' => 'max_value',
                    'label' => "Max value",
                ]);
                $this->crud->addField([
                    'name' => 'min_value',
                    'label' => "Min value",
                ]);
		// ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumn('min_value');
        $this->crud->removeColumn('max_value');
        $this->crud->removeColumn('variable_id');
        $this->crud->removeColumn('environment_id');
        $this->crud->addColumn([
                        'label' => trans('backpack::crud.environment_label'),
                        'type' => 'select',
                        'name' => 'environment_id',
                        'entity' => 'environment',
                        'attribute' => 'name',
                        'model' => "App\Models\Environment"
                    ]);
        $this->crud->removeColumn('element_id');
        $this->crud->addColumn([
                        'label' => trans('backpack::crud.variable_label'),
                        'type' => 'select',
                        'name' => 'variable_id',
                        'entity' => 'variable',
                        'attribute' => 'name',
                        'model' => "App\Models\Variable"
                    ]);
        $this->crud->addColumn([
                        'label' => 'Min value',
                        'name' => 'min_value',
                    ]);
        $this->crud->addColumn([
                        'label' => 'Max value',
                        'name' => 'max_value',
                    ]);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']);
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though: 
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable(); 

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
