<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ElementRequest as StoreRequest;
use App\Http\Requests\ElementRequest as UpdateRequest;
use Illuminate\Http\Request;

use App\Models\Element;
use App\Models\Criteria;
use App\Models\Variable;

class ElementCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->middleware(function ($request, $next) {
            if (session()->has('applocale') && in_array(session()->get('applocale'), ['es', 'en'])) {
                \App::setLocale(session()->get('applocale'));
                $this->crud->setEntityNameStrings(trans('backpack::crud.element_label'), trans('backpack::crud.elements_label'));
            }
            return $next($request);
        });
        
        $this->crud->setModel("App\Models\Element");
        $this->crud->setRoute("admin/element");
        $this->crud->setEntityNameStrings(trans('backpack::crud.element_label'), trans('backpack::crud.elements_label'));

        $this->crud->addButtonFromView('line', 'config', 'config', 'end');
        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();

        // ------ CRUD FIELDS
        $this->crud->addField([ // image
			'label' => "SVG File",
			'name' => "image",
			'type' => 'image',
			'upload' => true,
                        'disk' => 'uploads',
			'crop' => false, // set to true to allow cropping, false to disable
			'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
		]);
        
        $this->crud->addField([    // SELECT
                        'label' => "Category",
                        'type' => 'select2',
                        'name' => 'categories_id',
                        'entity' => 'category',
                        'attribute' => 'name',
                        'model' => "App\Models\Category"
                    ]);
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumn('categories_id');
        $this->crud->addColumn([
                        'label' => "Category",
                        'type' => 'select',
                        'name' => 'categories_id',
                        'entity' => 'category',
                        'attribute' => 'name',
                        'model' => "App\Models\Category"
                    ]);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']);
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['config', 'sending']);
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though: 
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable(); 

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}

	public function config($id)
	{
            $svg = null;
            $element = Element::find($id);
            $nodes = [];
            
            if(!is_null($element)) {
                // Instantiate new DOMDocument object
                $svg = new \DOMDocument();
                // Load SVG file from public folder
                $svg->load(asset('uploads').'/'.$element->image);
//                $this->buildDOMNode($svg, $nodes, 0);
                $logo = $svg->saveXML();
                $p = xml_parser_create();
                if(xml_parse_into_struct($p, $logo, $vals, $index) == 0) {
                    $vals = [];
                }
                    
                xml_parser_free($p);
            }
            return view('custom.elementconfig')
                        ->with('vals', $vals)
                        ->with('index', $index)
                        ->with('element', $element)
                    ;
        }
        
        public function save($id)
	{
            $svgModified = \Request::get('svg-for-update');
            if ($svgModified != '') {
                $criterias = \Request::get('criteria', []);
                $fills = \Request::get('fill');
                $translates = \Request::get('translate');
                $rotates = \Request::get('rotate');
                $scales = \Request::get('scale');
                $texts = \Request::get('text');

                $element = Element::find($id);
                
                foreach($criterias as $key => $criteria) {
                    $newCriteria = Criteria::create([
                                        'condition' => $criteria,
                                        'fill' => $fills[$key],
                                        'translate' => $translates[$key],
                                        'rotate' => $rotates[$key],
                                        'scale' => $scales[$key],
                                        'text' => $texts[$key],
                                        'elements_id' => $id,
                                    ]);
                    $element->criterias()->save($newCriteria);
                }

                $disk = "uploads";
                \Storage::disk($disk)->put( $element->image, $svgModified);
            }
            
            return redirect()->route('crud.element.index');
        }
        
        public function sending(Request $request, $id, $value)
	{
            $element = Element::find($id);
            $variables = Variable::all();

            if(!is_null($element)) {
                // Instantiate new DOMDocument object
                $svg = new \DOMDocument();
                // Load SVG file from public folder
                $svg->load(asset('uploads').'/'.$element->image);
                $text = $svg->saveXML($svg->documentElement);
                //$text = $svg->getElementsByTagName('svg').innerHTML;

                //If there is a control variable replace with the value sent
                foreach($variables as $variable) {
                    if(($variable->function_code == 5 || $variable->function_code == 6) && preg_match("/\s*+".preg_quote($variable->name)."\s*+/", $text)){
                        $variable->should_output = true;
                        $variable->value_output = $value;
                        $variable->save();
                    }
                }
            }
        }
}
