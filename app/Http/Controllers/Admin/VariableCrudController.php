<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\VariableRequest as StoreRequest;
use App\Http\Requests\VariableRequest as UpdateRequest;

class VariableCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\Variable");
        $this->crud->setRoute("admin/variable");
        $this->crud->setEntityNameStrings('variable', 'variables');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();

		// ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');
        $this->crud->removeField(['unit', 'function_code', 'registers_number', 'should_output', 'value_output', 'connection_id'], 'update/create/both');
        $this->crud->addField([    // SELECT
                        'name' => 'function_code',
                        'label' => "Type of variable",
                        'type' => 'select2_from_array',
                        'options' => [
                            1 => 'Digital I/O',
                            2 => 'Digital Input', 
                            3 => 'Register I/O',
                            4 => 'Register Input',
                            5 => 'Write Digital I/O',
                            6 => 'Write Register I/O'
                        ],
                        'allows_null' => false,
                    ]);
        $this->crud->addField([    // SELECT
                        'label' => "Connection",
                        'type' => 'select2',
                        'name' => 'connection_id',
                        'entity' => 'connection',
                        'attribute' => 'protocol',
                        'model' => "App\Models\Connection"
                    ]);
        
        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']);
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        $this->crud->removeColumns(['unit', 'function_code', 'registers_number', 'should_output', 'value_output', 'connection_id']);
        $this->crud->addColumn([
                        'label' => "Connection",
                        'type' => 'select',
                        'name' => 'connection_id',
                        'entity' => 'connection',
                        'attribute' => 'protocol',
                        'model' => "App\Models\Connection"
                    ]);
        $this->crud->addColumn(
        [
                        // run a function on the CRUD model and show its return value
                        'label' => "Type of variable", // Table column heading
                        'type' => "model_function",
                        'function_name' => 'getType', // the method in your Model
         ]);
        
        
        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though: 
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable(); 

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }
    
	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
