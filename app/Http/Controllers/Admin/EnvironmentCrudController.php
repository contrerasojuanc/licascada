<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Models\Environment;
use App\Models\Element;
use App\Models\Log;
use App\Models\Alarm;
use App\Models\Criterion;
use Illuminate\Support\Facades\Storage;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EnvironmentRequest as StoreRequest;
use App\Http\Requests\EnvironmentRequest as UpdateRequest;
use Illuminate\Http\Request;

use PHPModbus\PhpType;

use ModbusTcpClient\Network\BinaryStreamConnection;
use ModbusTcpClient\Packet\ModbusFunction\ReadCoilsRequest;
use ModbusTcpClient\Packet\ModbusFunction\ReadCoilsResponse;
use ModbusTcpClient\Packet\ModbusFunction\ReadHoldingRegistersRequest;
use ModbusTcpClient\Packet\ModbusFunction\ReadHoldingRegistersResponse;
use ModbusTcpClient\Packet\ModbusFunction\WriteSingleCoilRequest;
use ModbusTcpClient\Packet\ModbusFunction\WriteSingleCoilResponse;
use ModbusTcpClient\Packet\ModbusFunction\WriteSingleRegisterRequest;
use ModbusTcpClient\Packet\ModbusFunction\WriteSingleRegisterResponse;
use ModbusTcpClient\Packet\ResponseFactory;

class EnvironmentCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->middleware(function ($request, $next) {
            if (session()->has('applocale') && in_array(session()->get('applocale'), ['es', 'en'])) {
                \App::setLocale(session()->get('applocale'));
                $this->crud->setEntityNameStrings(trans('backpack::crud.environment_label'), trans('backpack::crud.environments_label'));
            }
            return $next($request);
        });
        
        $this->crud->setModel("App\Models\Environment");
        $this->crud->setRoute("admin/environment");
        $this->crud->setEntityNameStrings(trans('backpack::crud.environment_label'), trans('backpack::crud.environments_label'));
        
        $this->crud->addButtonFromView('line', 'config', 'config', 'end');
        $this->crud->addButtonFromView('line', 'log', 'log', 'end');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();

        // ------ CRUD FIELDS
        $this->crud->addField([    // SELECT
                        'label' => "Connection",
                        'type' => 'select2',
                        'name' => 'connection_id',
                        'entity' => 'connection',
                        'attribute' => 'protocol',
                        'model' => "App\Models\Connection"
                    ]);
                
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        $this->crud->removeColumn('connection_id');
        $this->crud->addColumn([
                        'label' => "Connection",
                        'type' => 'select',
                        'name' => 'connection_id',
                        'entity' => 'connection',
                        'attribute' => 'protocol',
                        'model' => "App\Models\Connection"
                    ]);
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']);
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD ACCESS
        $this->crud->allowAccess(['config', 'log', 'adquisition']);
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though: 
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable(); 

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
        
        public function config($id)
	{
            $environment = Environment::find($id);

            return view('custom.environmentconfig')
                        ->with('environment', $environment)
                    ;
        }
        
        public function save($id)
	{
            $jsonModified = \Request::get('json');
            
            if ($jsonModified != '') {
                $environment = Environment::find($id);

//                $environment->json = $jsonModified;
//                if($environment->save()) {
//                    return redirect()->route('config.environment', [
//                        'environment' => $environment,
//                    ])->with([
//                        'message_type' => 'sucess',
//                        'message' => trans('backpack::success'),
//                    ]);
//                }
                
                if(Storage::put('evironment'.$id.'.json', $jsonModified)) {
                    return redirect()->route('config.environment', [
                        'environment' => $environment,
                    ])->with([
                        'message_type' => 'sucess',
                        'message' => trans('backpack::success'),
                    ]);
                }
            }
            
            return redirect()->route('config.environment', [
                'environment' => $environment,
                
            ])->with([
                'message_type' => 'danger',
                'message' => trans('backpack::error'),
            ]);
        }
       
        public function log(Request $request, $id)
	{
            $environment = Environment::find($id);
            $logs = $environment->logs()
                    ->take(100)
                    ->orderBy('created_at', 'DESC')
                    ->get();
            
            if($request->ajax()) {
                return response()->json($logs->toArray());
            }
            return view('custom.environmentlog')
                        ->with([
                            'environment' => $environment,
                            'logs' => $logs,
                        ]);
        }
        
//        public function adquisition($id)
//	{
//            $value = [];
//            $variables = [];
//            $environment = Environment::findOrFail($id);
//            if($environment->connection()->exists()) {
//                $variables = $environment->connection->variables;
//            }
//            
//            foreach($variables as $variable) {
//                // Important slave modbus must to allow at leat 1000 connections at time in order to keep polling
//                $connection = BinaryStreamConnection::getBuilder()
//                                        ->setPort(502)
//                                        ->setHost($environment->connection->address)
//                                        ->build();
//                try {
//                    switch($variable->function_code) {
//                        case 1:
//                        case 2:
//                            // FC 1
//                            $packet = new ReadCoilsRequest($variable->start_address, $variable->registers_number);
//                            $binaryData = $connection->connect()->sendAndReceive($packet);
//                            $response = ResponseFactory::parseResponseOrThrow($binaryData);
//                            $value[$variable->id] = $response->getCoils();
//                            break;
//                        case 3:
//                        case 4:
//                            // FC 3
//                            $packet = new ReadHoldingRegistersRequest($variable->start_address, $variable->registers_number);
//                            $binaryData = $connection->connect()->sendAndReceive($packet);
//                            $response = ResponseFactory::parseResponseOrThrow($binaryData);
//                            $value[$variable->id] = $response->getData();
//                            break;
//                        case 5:
//                            // FC 5
//                            $packet = new WriteSingleCoilRequest($variable->start_address, $variable->value_output ? true : false);
//                            $binaryData = $connection->connect()->sendAndReceive($packet);
//                            $response = ResponseFactory::parseResponseOrThrow($binaryData);
//                            $value[$variable->id] = $response->isCoil();
//                            break;
//                        case 6:
//                            // FC 6
//                            $packet = new WriteSingleRegisterRequest($variable->start_address, $variable->value_output);
//                            $binaryData = $connection->connect()->sendAndReceive($packet);
//                            $response = ResponseFactory::parseResponseOrThrow($binaryData);
//                            $value[$variable->id] = $response->getValue();
//                            break;
//                    }
//                        
//                } catch (Exception $e) {
//                        return response()->json(['error' => true]);
//                }
//                $connection->close();
//            }
//
//            return response()->json([
//                'success' => true,
//                //'value' => json_encode($value),
//                'value' => $value,
//            ]);
//        }
        
        public function adquisition($id)
	{
            $result = [];
            $value = [];
            $variables = [];
            $environment = Environment::findOrFail($id);
            if($environment->connection()->exists()) {
                $variables = $environment->connection->variables;
            }
            
            foreach($variables as $variable) {
                try {
                    // Create Modbus object
                    $modbus = new \PHPModbus\ModbusMaster($environment->connection->address, "TCP");
                    $modbus->setSocketTimeout(0, 0.5);
                    $modbus->setTimeout(2);
                    switch($variable->function_code) {
                        case 1:
                            // FC 1
                            $value[$variable->name] = $modbus->readCoils(0, $variable->start_address, $variable->registers_number)[0];
                            break;
                        case 2:
                            // FC 2
                            $value[$variable->name] = $modbus->readInputDiscretes(0, $variable->start_address, $variable->registers_number)[0];
                            break;
                        case 3:
                            // FC 3
                            $values = $modbus->readMultipleRegisters(0, $variable->start_address, $variable->registers_number);
                            $values = array_chunk($values, 2);
                            $sum = 0;
                            foreach ($values as $bytes) {
                                $sum += PhpType::bytes2signedInt($bytes);
                            }
                            $value[$variable->name] = $sum;
                            break;
                        case 4:
                            // FC 4
                            $values = $modbus->readMultipleInputRegisters(0, $variable->start_address, $variable->registers_number);
                            $values = array_chunk($values, 2);
                            $sum = 0;
                            foreach ($values as $bytes) {
                                $sum += PhpType::bytes2signedInt($bytes);
                            }
                            $value[$variable->name] = $sum;
                            break;
                        case 5:
                            // FC 5
                            if($variable->should_output) {
                                $variable->should_output = false;
                                $variable->save();
                                $value[$variable->name] = $modbus->writeSingleCoil(0, $variable->start_address, [$variable->value_output]);
                            }
                            break;
                        case 6:
                            // FC 6
                            if($variable->should_output) {
                                $variable->should_output = false;
                                $variable->save();
                                $value[$variable->name] = $modbus->writeSingleRegister(0, $variable->start_address, [$variable->value_output]);
                            }
                            break;
                    }
                    
                    //Storing in database
                    if(isset($value[$variable->name])) {
                        $alarm = Alarm::where('environment_id', $environment->id)
                            ->where('variable_id', $variable->id)
                            ->first();
                    
                        $log = new Log(['value' => (int)$value[$variable->name]]);
                        $log->environment()->associate($environment);
                        $log->variable()->associate($variable);
                        if($alarm) {
                            $log->is_alarmed = (int)$value[$variable->name] < $alarm->min_value || (int)$value[$variable->name] > $alarm->max_value;
                        } 
                        $log->save();
                    
                        //Transforming for SVG representation
                        $result[$variable->name] = $value[$variable->name];
                        $criteria = Criterion::where('variable_id', $variable->id)
                                ->get();
                        foreach ($criteria as $criterion) {
                            switch($criterion->conditional_operator) {
                                case 0: // =
                                    if($value[$variable->name] == $criterion->conditional_operand)
                                       $result[$variable->name] = $criterion->conditional_result;
                                    break;
                                case 1: // >
                                    if($value[$variable->name] > $criterion->conditional_operand)
                                       $result[$variable->name] = $criterion->conditional_result;
                                    break;
                                case 2: // >=
                                    if($value[$variable->name] >= $criterion->conditional_operand)
                                       $result[$variable->name] = $criterion->conditional_result;
                                    break;
                                case 3: // <
                                    if($value[$variable->name] < $criterion->conditional_operand)
                                       $result[$variable->name] = $criterion->conditional_result;
                                    break;
                                case 4: // <=
                                    if($value[$variable->name] <= $criterion->conditional_operand)
                                       $result[$variable->name] = $criterion->conditional_result;
                                    break;
                            }
                        }
                    }
            
                } catch (Exception $e) {
                        return response()->json(['error' => true]);
                }
            }

            return response()->json([
                'success' => true,
                'value' => $result,
                'original' => $value,
            ]);
        }
}
