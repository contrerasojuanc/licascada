<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;

class LayoutController extends Controller
{
    public function changeLanguage($language) 
    {
        if(in_array($language, ['es','en'])){
            Session::put('applocale', $language);
            \App::setLocale($language);
        }
        return redirect()->back();
    }
}
