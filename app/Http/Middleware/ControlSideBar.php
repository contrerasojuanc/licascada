<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Category;

class ControlSideBar
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        \DB::enableQueryLog();
        $categories = Category::all();
        \View::share(['categories' => $categories]);
//        dd(\DB::getQueryLog());
        return $next($request);
    }
}
