<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('applocale') && in_array(Session::get('applocale'), ['es', 'en'])) {
            \App::setLocale(Session::get('applocale'));
        }
        
        return $next($request);
    }
}
