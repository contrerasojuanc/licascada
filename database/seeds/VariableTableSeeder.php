<?php

use Illuminate\Database\Seeder;

class VariableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variables')->insert([
            'name' => '$status',
            'description' => 'Status engine', //On, off
            'function_code' => '1', //Modbus FC 01 Read Coils (I/O)
            'start_address' => '0',
            'connection_id' => '1',
        ]);
        DB::table('variables')->insert([
            'name' => '$level',
            'description' => 'Level of liquid',
            'unit' => 'meter',
            'function_code' => '3', //Modbus FC 03 Read Holding Register (I/O)
            'start_address' => '0', 
            'registers_number' => '2', 
            'connection_id' => '1',
        ]);
        DB::table('variables')->insert([
            'name' => '$speed',
            'description' => 'Speed of rotation',
            'unit' => 'meter',
            'function_code' => '3', //Modbus FC 03 Read Holding Register (I/O)
            'start_address' => '2', 
            'registers_number' => '2', 
            'connection_id' => '1',
        ]);
        DB::table('variables')->insert([
            'name' => '$start',
            'description' => 'Start engine',
            'function_code' => '5', //Modbus FC 05 Write single coil
            'start_address' => '0',
            'value_output' => '1',
            'connection_id' => '1',
        ]);
        DB::table('variables')->insert([
            'name' => '$thermostat',
            'description' => 'Set thermostat',
            'unit' => 'degree',
            'function_code' => '6', //Modbus FC 06 Write Single Register
            'start_address' => '0', 
            'registers_number' => '2', 
            'value_output' => '-90',
            'connection_id' => '1',
        ]);
        DB::table('variables')->insert([
            'name' => '$original_level',
            'description' => 'Level without transformation',
            'unit' => 'meter',
            'function_code' => '3', //Modbus FC 03 Read Holding Register (I/O)
            'start_address' => '0', 
            'registers_number' => '2', 
            'connection_id' => '1',
        ]);
    }
}
