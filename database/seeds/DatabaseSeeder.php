<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EnvironmentTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ElementTableSeeder::class);
        $this->call(ConnectionTableSeeder::class);
        $this->call(VariableTableSeeder::class);
        $this->call(AlarmTableSeeder::class);
        $this->call(CriterionTableSeeder::class);
    }
}
