<?php

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elements')->insert([
            'name' => 'Actuator',
            'image' => 'svg/actuator.svg',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Agitator',
            'image' => 'svg/agitator.svg',
            'categories_id' => Category::where('name', 'Mixing')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Atmospheric Tank',
            'image' => 'svg/atmospheric_tank.svg',
            'type' => 'Storage',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Conveyor',
            'image' => 'svg/conveyor.svg',
            'categories_id' => Category::where('name', 'Material handling')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Distillation Tower',
            'image' => 'svg/distillation_tower.svg',
            'type' => 'Process',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Finned Exchanger',
            'image' => 'svg/finned_exchanger.svg',
            'categories_id' => Category::where('name', 'Heating ventilating & Air Conditioning')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Floating Roof Tank',
            'image' => 'svg/floating_roof_tank.svg',
            'type' => 'Storage',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Jacketed Vessel',
            'image' => 'svg/jacketed_vessel.svg',
            'type' => 'Process',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Liquid Filter',
            'image' => 'svg/liquid_filter.svg',
            'categories_id' => Category::where('name', 'Filters')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Pressure Storage Vessel',
            'image' => 'svg/pressure_storage_vessel.svg',
            'type' => 'Storage',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Rotary Feeder',
            'image' => 'svg/rotary_feeder.svg',
            'categories_id' => Category::where('name', 'Material handling')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Turbine',
            'image' => 'svg/turbine.svg',
            'categories_id' => Category::where('name', 'Rotating equipment')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Valve',
            'image' => 'svg/valve.svg',
            'type' => 'Valves',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Blower',
            'image' => 'svg/blower.svg',
            'categories_id' => Category::where('name', 'Rotating equipment')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Butterfly Valve',
            'image' => 'svg/butterfly_valve.svg',
            'type' => 'Valves',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Check Valve',
            'image' => 'svg/check_valve.svg',
            'type' => 'Valves',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Gauge Visual',
            'image' => 'svg/gauge_visual.svg',
            'type' => 'Gauges',
            'categories_id' => Category::where('name', 'General')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Led Button',
            'image' => 'svg/led_button.svg',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Speed Meter',
            'image' => 'svg/speed_meter.svg',
            'categories_id' => Category::where('name', 'Valves and Actuators')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Container Visual',
            'image' => 'svg/container_visual.svg',
            'type' => 'Storage',
            'categories_id' => Category::where('name', 'Containers and Vessels')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('elements')->insert([
            'name' => 'Indicator Visual',
            'image' => 'svg/indicator_visual.svg',
            'type' => 'Labels',
            'categories_id' => Category::where('name', 'General')->get(['id'])->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
