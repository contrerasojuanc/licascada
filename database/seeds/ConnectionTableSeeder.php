<?php

use Illuminate\Database\Seeder;

class ConnectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('connections')->insert([
            'protocol' => 'Modbus TCP',
            'address' => '127.0.0.1',
            'frequency' => '1000', //In miliseconds
            'bit_per_sec' => 0,
            'parity' => 0,
            'data_bits' => 0,
            'stop_bits' => 0,
            'flow_control' => 0,
        ]);
        
        DB::table('connections')->insert([
            'protocol' => 'Modbus Serial',
            'address' => 'COM1',
            'frequency' => '1000', //In miliseconds
        ]);
    }
}
