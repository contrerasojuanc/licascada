<?php

use Illuminate\Database\Seeder;

class EnvironmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('environments')->insert([
            'name' => 'Demo',
            'connection_id' => 1,
        ]);
    }
}
