<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AlarmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alarms')->insert([
            'environment_id' => 1,
            'variable_id' => 2,
            'min_value' => 0,
            'max_value' => 65,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
