<?php

use Illuminate\Database\Seeder;
use App\Models\Variable;

class CriterionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $status variable Section
        $variable_id = Variable::where('name', '$status')->get(['id'])->first()->id;
        DB::table('criteria')->insert([
            'conditional_operator' => 0,
            'conditional_operand' => 0,
            'conditional_result' => '#800000',
            'variable_id' => $variable_id, //Variable $status
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 0,
            'conditional_operand' => 1,
            'conditional_result' => '#008000',
            'variable_id' => $variable_id, //Variable $status
        ]);
        // $status variable Section 
        
        // $speed variable Section 
        $variable_id = Variable::where('name', '$speed')->get(['id'])->first()->id;
        DB::table('criteria')->insert([
            'conditional_operator' => 4, //<=
            'conditional_operand' => 0,
            'conditional_result' => 59,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 1,
            'conditional_result' => 90,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 11,
            'conditional_result' => 121,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 21,
            'conditional_result' => 152,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 31,
            'conditional_result' => 183,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 41,
            'conditional_result' => 214,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 51,
            'conditional_result' => 245,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 61,
            'conditional_result' => 276,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 71,
            'conditional_result' => 307,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 81,
            'conditional_result' => 338,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 91,
            'conditional_result' => 369,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        // $speed variable Section
        
        // $level variable Section 
        $variable_id = Variable::where('name', '$level')->get(['id'])->first()->id;
        DB::table('criteria')->insert([
            'conditional_operator' => 4, //<=
            'conditional_operand' => 0,
            'conditional_result' => 0,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 1,
            'conditional_result' => 38,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 11,
            'conditional_result' => 77,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 21,
            'conditional_result' => 115,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 31,
            'conditional_result' => 154,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 41,
            'conditional_result' => 192,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 51,
            'conditional_result' => 231,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 61,
            'conditional_result' => 269,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 71,
            'conditional_result' => 308,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 81,
            'conditional_result' => 346,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        DB::table('criteria')->insert([
            'conditional_operator' => 2, //>=
            'conditional_operand' => 91,
            'conditional_result' => 385,
            'variable_id' => $variable_id, //Variable $speed
        ]);
        // $level variable Section
    }
}
