<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id');
            $table->text('protocol'); //Modbus TCP, Modbus serial
            $table->text('address'); //IP address or (Windows) COM1  (Linux) /dev/ttyUSB0
            $table->text('frequency')->nullable();
            $table->integer('bit_per_sec')->default(9600)->nullable(); //baud 9800
            $table->string('parity',20)->default('none')->nullable(); 
            $table->integer('data_bits')->default(8)->nullable(); 
            $table->integer('stop_bits')->default(1)->nullable(); 
            $table->string('flow_control',20)->default('none')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('connections');
    }
}